//
//  main.m
//  borrar2
//
//  Created by silenGSR on 7/4/16.
//  Copyright © 2016 silenGSR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
